## Updating Q-Client & Docker Images

In case of severe updates to the Q-Client, you will be required to update the validator files and configs. To do so, within directory `/testnet-validator` (for validator) or `/testnet-rootnode` (for rootnode) or `testnet-fullnode` (for fullnode), use the following commands:

1. Stash your changes and pull the latest configs
```text
$ git stash && git pull
```

1.1. Alternatively you can change the docker image directly in your **.env** file:
```
...
QCLIENT_IMAGE=qblockchain/q-client:1.2.1
...
```

2. Apply your stashed changes and pull (and overwrite) the latest docker images
```text
$ git stash apply && docker-compose pull
```

3. Restart with new configs & images
```text
$ docker-compose up -d
```

Now your validator node should restart and synchronise with the testnet again.
