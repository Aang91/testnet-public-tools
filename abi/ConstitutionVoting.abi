[
	{
		"inputs": [],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "_id",
				"type": "uint256"
			},
			{
				"components": [
					{
						"internalType": "string",
						"name": "remark",
						"type": "string"
					},
					{
						"components": [
							{
								"internalType": "uint256",
								"name": "votingStartTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "votingEndTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "vetoEndTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "proposalExecutionP",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredQuorum",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredMajority",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredSMajority",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredSQuorum",
								"type": "uint256"
							}
						],
						"internalType": "struct IVoting.VotingParams",
						"name": "params",
						"type": "tuple"
					},
					{
						"components": [
							{
								"internalType": "uint256",
								"name": "weightFor",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "weightAgainst",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "vetosCount",
								"type": "uint256"
							}
						],
						"internalType": "struct IVoting.VotingCounters",
						"name": "counters",
						"type": "tuple"
					},
					{
						"internalType": "bool",
						"name": "executed",
						"type": "bool"
					}
				],
				"indexed": false,
				"internalType": "struct IVoting.BaseProposal",
				"name": "_proposal",
				"type": "tuple"
			}
		],
		"name": "ProposalCreated",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			},
			{
				"indexed": false,
				"internalType": "bytes32",
				"name": "_constitutionHash",
				"type": "bytes32"
			}
		],
		"name": "ProposalExecuted",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			}
		],
		"name": "QuorumReached",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			},
			{
				"indexed": false,
				"internalType": "enum IVoting.VotingOption",
				"name": "_votingOption",
				"type": "uint8"
			},
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "_amount",
				"type": "uint256"
			}
		],
		"name": "UserVoted",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "sender",
				"type": "address"
			}
		],
		"name": "VetoOccurred",
		"type": "event"
	},
	{
		"inputs": [],
		"name": "constitutionHash",
		"outputs": [
			{
				"internalType": "bytes32",
				"name": "",
				"type": "bytes32"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"name": "hasRootVetoed",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"name": "hasUserVoted",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "proposalCounter",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"name": "proposals",
		"outputs": [
			{
				"components": [
					{
						"internalType": "string",
						"name": "remark",
						"type": "string"
					},
					{
						"components": [
							{
								"internalType": "uint256",
								"name": "votingStartTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "votingEndTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "vetoEndTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "proposalExecutionP",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredQuorum",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredMajority",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredSMajority",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredSQuorum",
								"type": "uint256"
							}
						],
						"internalType": "struct IVoting.VotingParams",
						"name": "params",
						"type": "tuple"
					},
					{
						"components": [
							{
								"internalType": "uint256",
								"name": "weightFor",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "weightAgainst",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "vetosCount",
								"type": "uint256"
							}
						],
						"internalType": "struct IVoting.VotingCounters",
						"name": "counters",
						"type": "tuple"
					},
					{
						"internalType": "bool",
						"name": "executed",
						"type": "bool"
					}
				],
				"internalType": "struct IVoting.BaseProposal",
				"name": "base",
				"type": "tuple"
			},
			{
				"internalType": "uint256",
				"name": "parametersSize",
				"type": "uint256"
			},
			{
				"internalType": "enum ConstitutionVoting.Classification",
				"name": "classification",
				"type": "uint8"
			},
			{
				"internalType": "bytes32",
				"name": "newConstitutionHash",
				"type": "bytes32"
			},
			{
				"internalType": "bytes32",
				"name": "currentConstitutionHash",
				"type": "bytes32"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes32",
				"name": "_constitutionHash",
				"type": "bytes32"
			},
			{
				"internalType": "address",
				"name": "_registry",
				"type": "address"
			}
		],
		"name": "initialize",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_id",
				"type": "uint256"
			}
		],
		"name": "getConstitutionHash",
		"outputs": [
			{
				"internalType": "bytes32",
				"name": "",
				"type": "bytes32"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "_remark",
				"type": "string"
			},
			{
				"internalType": "enum ConstitutionVoting.Classification",
				"name": "_proposalType",
				"type": "uint8"
			},
			{
				"internalType": "bytes32",
				"name": "_hashValue",
				"type": "bytes32"
			},
			{
				"internalType": "string",
				"name": "_paramKey",
				"type": "string"
			},
			{
				"internalType": "address",
				"name": "_paramValue",
				"type": "address"
			}
		],
		"name": "createAddrProposal",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "_remark",
				"type": "string"
			},
			{
				"internalType": "enum ConstitutionVoting.Classification",
				"name": "_proposalType",
				"type": "uint8"
			},
			{
				"internalType": "bytes32",
				"name": "_hashValue",
				"type": "bytes32"
			},
			{
				"internalType": "string",
				"name": "_paramKey",
				"type": "string"
			},
			{
				"internalType": "bool",
				"name": "_paramValue",
				"type": "bool"
			}
		],
		"name": "createBoolProposal",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "_remark",
				"type": "string"
			},
			{
				"internalType": "enum ConstitutionVoting.Classification",
				"name": "_proposalType",
				"type": "uint8"
			},
			{
				"internalType": "bytes32",
				"name": "_hashValue",
				"type": "bytes32"
			},
			{
				"internalType": "string",
				"name": "_paramKey",
				"type": "string"
			},
			{
				"internalType": "bytes32",
				"name": "_paramValue",
				"type": "bytes32"
			}
		],
		"name": "createBytesProposal",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "_remark",
				"type": "string"
			},
			{
				"internalType": "enum ConstitutionVoting.Classification",
				"name": "_proposalType",
				"type": "uint8"
			},
			{
				"internalType": "bytes32",
				"name": "_hashValue",
				"type": "bytes32"
			},
			{
				"internalType": "string",
				"name": "_paramKey",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "_paramValue",
				"type": "string"
			}
		],
		"name": "createStrProposal",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "_remark",
				"type": "string"
			},
			{
				"internalType": "enum ConstitutionVoting.Classification",
				"name": "_proposalType",
				"type": "uint8"
			},
			{
				"internalType": "bytes32",
				"name": "_hashValue",
				"type": "bytes32"
			},
			{
				"internalType": "string",
				"name": "_paramKey",
				"type": "string"
			},
			{
				"internalType": "uint32",
				"name": "_paramValue",
				"type": "uint32"
			}
		],
		"name": "createUintProposal",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "_remark",
				"type": "string"
			},
			{
				"internalType": "enum ConstitutionVoting.Classification",
				"name": "_proposalType",
				"type": "uint8"
			},
			{
				"internalType": "bytes32",
				"name": "_hashValue",
				"type": "bytes32"
			},
			{
				"components": [
					{
						"internalType": "string",
						"name": "paramKey",
						"type": "string"
					},
					{
						"internalType": "enum IParametersVoting.ParameterType",
						"name": "paramType",
						"type": "uint8"
					},
					{
						"internalType": "address",
						"name": "addrValue",
						"type": "address"
					},
					{
						"internalType": "bool",
						"name": "boolValue",
						"type": "bool"
					},
					{
						"internalType": "bytes32",
						"name": "bytes32Value",
						"type": "bytes32"
					},
					{
						"internalType": "string",
						"name": "strValue",
						"type": "string"
					},
					{
						"internalType": "uint256",
						"name": "uintValue",
						"type": "uint256"
					}
				],
				"internalType": "struct ConstitutionVoting.ParameterInfo[]",
				"name": "_parametersArr",
				"type": "tuple[]"
			}
		],
		"name": "createProposal",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_id",
				"type": "uint256"
			}
		],
		"name": "getStatus",
		"outputs": [
			{
				"internalType": "enum IVoting.ProposalStatus",
				"name": "",
				"type": "uint8"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "execute",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "voteFor",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "voteAgainst",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_id",
				"type": "uint256"
			}
		],
		"name": "veto",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "getParametersArr",
		"outputs": [
			{
				"components": [
					{
						"internalType": "string",
						"name": "paramKey",
						"type": "string"
					},
					{
						"internalType": "enum IParametersVoting.ParameterType",
						"name": "paramType",
						"type": "uint8"
					},
					{
						"internalType": "address",
						"name": "addrValue",
						"type": "address"
					},
					{
						"internalType": "bool",
						"name": "boolValue",
						"type": "bool"
					},
					{
						"internalType": "bytes32",
						"name": "bytes32Value",
						"type": "bytes32"
					},
					{
						"internalType": "string",
						"name": "strValue",
						"type": "string"
					},
					{
						"internalType": "uint256",
						"name": "uintValue",
						"type": "uint256"
					}
				],
				"internalType": "struct ConstitutionVoting.ParameterInfo[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "getProposal",
		"outputs": [
			{
				"components": [
					{
						"internalType": "string",
						"name": "remark",
						"type": "string"
					},
					{
						"components": [
							{
								"internalType": "uint256",
								"name": "votingStartTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "votingEndTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "vetoEndTime",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "proposalExecutionP",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredQuorum",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredMajority",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredSMajority",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "requiredSQuorum",
								"type": "uint256"
							}
						],
						"internalType": "struct IVoting.VotingParams",
						"name": "params",
						"type": "tuple"
					},
					{
						"components": [
							{
								"internalType": "uint256",
								"name": "weightFor",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "weightAgainst",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "vetosCount",
								"type": "uint256"
							}
						],
						"internalType": "struct IVoting.VotingCounters",
						"name": "counters",
						"type": "tuple"
					},
					{
						"internalType": "bool",
						"name": "executed",
						"type": "bool"
					}
				],
				"internalType": "struct IVoting.BaseProposal",
				"name": "",
				"type": "tuple"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "getProposalStats",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "requiredQuorum",
						"type": "uint256"
					},
					{
						"internalType": "uint256",
						"name": "currentQuorum",
						"type": "uint256"
					},
					{
						"internalType": "uint256",
						"name": "requiredMajority",
						"type": "uint256"
					},
					{
						"internalType": "uint256",
						"name": "currentMajority",
						"type": "uint256"
					},
					{
						"internalType": "uint256",
						"name": "currentVetoPercentage",
						"type": "uint256"
					}
				],
				"internalType": "struct IVoting.VotingStats",
				"name": "",
				"type": "tuple"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "getVotingWeightInfo",
		"outputs": [
			{
				"components": [
					{
						"internalType": "bool",
						"name": "hasAlreadyVoted",
						"type": "bool"
					},
					{
						"internalType": "bool",
						"name": "canVote",
						"type": "bool"
					},
					{
						"components": [
							{
								"internalType": "uint256",
								"name": "ownWeight",
								"type": "uint256"
							},
							{
								"internalType": "address",
								"name": "votingAgent",
								"type": "address"
							},
							{
								"internalType": "enum DelegationStatus",
								"name": "delegationStatus",
								"type": "uint8"
							},
							{
								"internalType": "uint256",
								"name": "lockedUntil",
								"type": "uint256"
							}
						],
						"internalType": "struct BaseVotingWeightInfo",
						"name": "base",
						"type": "tuple"
					}
				],
				"internalType": "struct IQthVoting.VotingWeightInfo",
				"name": "",
				"type": "tuple"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "getVetosNumber",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_proposalId",
				"type": "uint256"
			}
		],
		"name": "getVetosPercentage",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	}
]